﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Web
{
    public class TestHub : Hub
    {

        public static List<Guid> ActiveGuids { get; set; } = new List<Guid>();

        public void Echo(Guid guid, string text)
        {
            Clients.All.Echo(guid, text);
        }
        public void JoinUser(Guid guid, string id)
        {
            ActiveGuids.Add(guid);
            Clients.All.JoinUser(guid);
            GetActiveUsers(id);
        }
        public void LeftUser(Guid guid)
        {
            ActiveGuids.Remove(guid);
            Clients.All.LeftUser(guid);
        }

        public void GetActiveUsers(string id)
        {
            Clients.Client(id).GetActiveUsers(ActiveGuids);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }
    }
}