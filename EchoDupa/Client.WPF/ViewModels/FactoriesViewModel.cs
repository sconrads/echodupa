﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using Client.WPF.Properties;

namespace Client.WPF.ViewModels
{
    public class FactoriesViewModel : BindableBase
    {
        public FactoriesViewModel()
        {

        }

        public string HeaderInfo => Common.ResourcesPL.Faktories;
    }
}