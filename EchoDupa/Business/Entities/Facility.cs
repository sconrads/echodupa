﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Business.Entities
{
    [Serializable]
    public class Facility
    {
        public string Name { get; set; }
        public double Efficiency { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }

        public long UpgradeCost => (long)Math.Pow(10, Level);

        public void IncreaseLevel(ref long money)
        {
            if(UpgradeCost > money)
                throw new Exceptions.FacilityUpgradeTooExpensiveException();

            Level++;
            Efficiency = (Level);
        }

        public static long GetNewFacilityCost(int countFacility)
        {
            return 10 ^ countFacility;
        }

        public void ChangePeopleCount(long peopleToChange, ref long nrOfPeople)
        {
            if (peopleToChange > nrOfPeople)
            {
                NrOfWorkers += nrOfPeople;
                nrOfPeople = 0;
            }
            else if (peopleToChange < NrOfWorkers)
            {
                nrOfPeople = NrOfWorkers;
                NrOfWorkers = 0;
            }
            else
            {
                NrOfWorkers += peopleToChange;
                nrOfPeople -= peopleToChange;
            }
        }
    }
}
