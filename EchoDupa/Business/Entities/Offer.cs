﻿using System;

namespace Business.Entities
{
    public class Offer
    {
        public Ware Ware { get; set; }
        public long Quantity { get; set; }
        public double UnitPrice { get; set; }
    }
}