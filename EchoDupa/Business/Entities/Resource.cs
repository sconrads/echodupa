﻿using System;

namespace Business.Entities
{
    [Serializable]
    public class Resource : Ware
    {
        public double MiningSpeed { get; set; }
    }
}