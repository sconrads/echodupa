﻿using System;

namespace Business.Entities
{
    [Serializable]
    public class Ware
    {
        public string Name { get; set; }
        public double StorageRatio { get; set; }//współczynnik wielkości towaru (np platyna 0.01, kamień 10000)
        public int QualityLevel { get; set; }
    }
}