﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    [Serializable]
    public class Product : Ware
    {
        public double ProductionSpeed { get; set; }
        public Dictionary<Ware, long> Components { get; set; }
    }
}