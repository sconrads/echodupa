﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    [Serializable]
    public class Warehouse
    {
        public List<WarehouseContainer> Containers { get; set; }
        public int Level { get; set; }

    }
}