﻿using System;
using System.Runtime.Serialization;

namespace Business.Exceptions
{
    public class FacilityTooExpensiveExeption : Exception
    {
        public FacilityTooExpensiveExeption()
        {
        }

        public FacilityTooExpensiveExeption(string message) : base(message)
        {
        }

        public FacilityTooExpensiveExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FacilityTooExpensiveExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}