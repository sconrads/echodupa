﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;


namespace Common.Dictionaries
{
    public static class Wares
    {

        public static Resource Coal = new Resource()
        {
            Name = ResourcesPL.ResourceCoal,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Dimond = new Resource()
        {
            Name = ResourcesPL.ResourceDimond,
            MiningSpeed = 0.001,
            QualityLevel = 1,
            StorageRatio = 1
        };

        public static Resource Food = new Resource()
        {
            Name = ResourcesPL.ResourceFood,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Stone = new Resource()
        {
            Name = ResourcesPL.ResourceStone,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Wood = new Resource()
        {
            Name = ResourcesPL.ResourceWood,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Iron = new Resource()
        {
            Name = ResourcesPL.ResourceIron,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Gold = new Resource()
        {
            Name = ResourcesPL.ResourceGold,
            MiningSpeed = 0.001,
            QualityLevel = 0,
            StorageRatio = 1
        };



        public static List<Product> Products = new List<Product>()
        {
            new Product()
            {
                Name = ResourcesPL.ProductForniture, 
                Components = new List<Dictionary<Ware, int>>() {Wood, Iron}
            }
        };
    }
}
