﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;


namespace Common.Dictionaries
{
    public static class Wares
    {
        public static List<Resource> Resources = new List<Resource>()
        {
            new Resource() {Name = ResourcesPL.ResourceCoal,MiningSpeed = 50, QualityLevel = 0, StorageRatio = 1000},
            new Resource() {Name = ResourcesPL.ResourceDimond,MiningSpeed = 0.001, QualityLevel = 1, StorageRatio = 1},
            new Resource() {Name = ResourcesPL.ResourceFood,MiningSpeed = 50, QualityLevel = 0, StorageRatio = 1000},
            new Resource() {Name = ResourcesPL.ResourceStone,MiningSpeed = 50, QualityLevel = 0, StorageRatio = 1000},
            new Resource() {Name = ResourcesPL.ResourceWood,MiningSpeed = 50, QualityLevel = 0, StorageRatio = 1000},
            new Resource() {Name = ResourcesPL.ResourceGold,MiningSpeed = 0.001, QualityLevel = 0, StorageRatio = 1},
        };
    }
}
