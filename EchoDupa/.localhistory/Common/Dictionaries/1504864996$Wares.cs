﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;


namespace Common.Dictionaries
{
    public static class Wares
    {
        public static Dictionary<string, Resource> ResourcesDictionary = new Dictionary<string, Resource>()
        {
            {
                ResourcesPL.ResourceCoal, new Resource()
                {
                    Name = ResourcesPL.ResourceCoal,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceGold, new Resource()
                {
                    Name = ResourcesPL.ResourceGold,
                    MiningSpeed = 0.001,
                    QualityLevel = 0,
                    StorageRatio = 1
                }
            },
            {
                ResourcesPL.ResourceClay, new Resource()
                {
                    Name = ResourcesPL.ResourceClay,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceIron, new Resource()
                {
                    Name = ResourcesPL.ResourceIron,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceWood, new Resource()
                {
                    Name = ResourcesPL.ResourceWood,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceStone, new Resource()
                {
                    Name = ResourcesPL.ResourceStone,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceFood, new Resource()
                {
                    Name = ResourcesPL.ResourceFood,
                    MiningSpeed = 50,
                    QualityLevel = 0,
                    StorageRatio = 1000
                }
            },
            {
                ResourcesPL.ResourceDimond, new Resource()
                {
                    Name = ResourcesPL.ResourceDimond,
                    MiningSpeed = 0.001,
                    QualityLevel = 1,
                    StorageRatio = 1
                }
            }
        };


        public static Dictionary<string, Product> ProductsDictionary = new Dictionary<string, Product>()
        {
            {
                ResourcesPL.ProductForniture, new Product()
                {
                    Name = ResourcesPL.ProductForniture,
                    Components =
                        new Dictionary<Ware, long>()
                        {
                            {ResourcesDictionary[ResourcesPL.ResourceWood], 10},
                            {ResourcesDictionary[ResourcesPL.ResourceIron], 2}
                        },
                    StorageRatio = 10,
                    QualityLevel = 2,
                    ProductionSpeed = 0.3
                }
            },
            {
                ResourcesPL.ProductTools, new Product()
                {
                    Name = ResourcesPL.ProductTools,
                    Components =
                        new Dictionary<Ware, long>()
                        {
                            {ResourcesDictionary[ResourcesPL.ResourceWood], 2},
                            {ResourcesDictionary[ResourcesPL.ResourceIron], 15},
                            {ResourcesDictionary[ResourcesPL.ResourceCoal], 5}
                        },
                    StorageRatio = 20,
                    QualityLevel = 2,
                    ProductionSpeed = 0.5
                }
            },
            {
                ResourcesPL.ProductPottery, new Product()
                {
                    Name = ResourcesPL.ProductPottery,
                    Components =
                        new Dictionary<Ware, long>()
                        {
                            {ResourcesDictionary[ResourcesPL.ResourceWood], 10},
                            {ResourcesDictionary[ResourcesPL.ResourceClay], 10}
                        },
                    StorageRatio = 10,
                    QualityLevel = 2,
                    ProductionSpeed = 0.3
                }
            },
            {
                ResourcesPL.ProductSteel, new Product()
                {
                    Name = ResourcesPL.ProductSteel,
                    Components =
                        new Dictionary<Ware, long>()
                        {
                            {ResourcesDictionary[ResourcesPL.ResourceCoal], 10},
                            {ResourcesDictionary[ResourcesPL.ResourceIron], 30}
                        },
                    StorageRatio = 30,
                    QualityLevel = 2,
                    ProductionSpeed = 0.5
                }
            },
            {
                ResourcesPL.ProductAdvancedTools, new Product()
                {
                    Name = ResourcesPL.ProductAdvancedTools,
                    Components =
                        new Dictionary<Ware, long>()
                        {
                            {ResourcesDictionary[ResourcesPL.ResourceCoal], 10},
                            {ResourcesDictionary[ResourcesPL.ResourceIron], 30},
                            {ProductsDictionary[ResourcesPL.ProductSteel], 5}
                        },
                    StorageRatio = 10,
                    QualityLevel = 3,
                    ProductionSpeed = 0.1
                }
            }
        };

    }
}