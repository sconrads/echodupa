﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;


namespace Common.Dictionaries
{
    public static class Wares
    {

        public static Resource Coal = new Resource()
        {
            Name = ResourcesPL.ResourceCoal,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Dimond = new Resource()
        {
            Name = ResourcesPL.ResourceDimond,
            MiningSpeed = 0.001,
            QualityLevel = 1,
            StorageRatio = 1
        };

        public static Resource Food = new Resource()
        {
            Name = ResourcesPL.ResourceFood,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Stone = new Resource()
        {
            Name = ResourcesPL.ResourceStone,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Wood = new Resource()
        {
            Name = ResourcesPL.ResourceWood,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Iron = new Resource()
        {
            Name = ResourcesPL.ResourceIron,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Clay = new Resource()
        {
            Name = ResourcesPL.ResourceClay,
            MiningSpeed = 50,
            QualityLevel = 0,
            StorageRatio = 1000
        };

        public static Resource Gold = new Resource()
        {
            Name = ResourcesPL.ResourceGold,
            MiningSpeed = 0.001,
            QualityLevel = 0,
            StorageRatio = 1
        };



        public static Product Forniuture = new Product()
        {
            Name = ResourcesPL.ProductForniture,
            Components = new Dictionary<Ware, long>(){ {Wood, 10}, {Iron, 2} },
            StorageRatio = 10,
            QualityLevel = 2,
            ProductionSpeed = 0.3
        };

        public static Product Tools = new Product()
        {
            Name = ResourcesPL.ProductTools,
            Components = new Dictionary<Ware, long>() { { Wood, 2 }, { Iron, 15 }, { Coal, 5 }},
            StorageRatio = 20,
            QualityLevel = 2,
            ProductionSpeed = 0.5
        };

        public static Product Pottery = new Product()
        {
            Name = ResourcesPL.ProductPottery,
            Components = new Dictionary<Ware, long>() { { Wood, 10 }, { Clay, 10 } },
            StorageRatio = 10,
            QualityLevel = 2,
            ProductionSpeed = 0.3
        };

        public static Product Steel = new Product()
        {
            Name = ResourcesPL.ProductSteel,
            Components = new Dictionary<Ware, long>() { { Coal, 10 }, { Iron, 30 } },
            StorageRatio = 30,
            QualityLevel = 2,
            ProductionSpeed = 0.5
        };

        public static Product AdvancedTools = new Product()
        {
            Name = ResourcesPL.ProductAdvancedTools,
            Components = new Dictionary<Ware, long>() { { Coal, 10 }, { Iron, 30 }, { Steel, 5 }},
            StorageRatio = 10,
            QualityLevel = 3,
            ProductionSpeed = 0.1
        };
    }
}
