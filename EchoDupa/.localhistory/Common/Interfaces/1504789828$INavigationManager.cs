﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;

namespace Common.Interfaces
{
    public interface INavigationManager
    {
        Action ShowSetupPlayerAction { get; set; }
        Player ShowSetupPlayerDialog();
    }
}
