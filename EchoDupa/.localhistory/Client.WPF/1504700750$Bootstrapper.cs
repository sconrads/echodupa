﻿using System;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Client.WPF.Views;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;

namespace Client.WPF
{
    class Bootstrapper : UnityBootstrapper
    {
        private IRegionManager _regionManager;

        public Bootstrapper()
        {
            
        }
        
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            RegisterTypeIfMissing(typeof(IRegionManager), typeof(RegionManager), true);

            Application.Current.MainWindow.Show();

            _regionManager = Container.Resolve<IRegionManager>();
            _regionManager.RegisterViewWithRegion("InfoRegion", typeof(InfoView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(FactoriesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(MinesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(WarehousesView));
        }
    }
}
