﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Entities;
using Common.Interfaces;
using Prism.Interactivity.InteractionRequest;

namespace Client.WPF.Navigation
{
    public class NavigationManager : INavigationManager
    {
        public Action ShowSetupPlayerAction { get; set; }
        public NavigationManager( )
        {
            
        }
        public Player ShowSetupPlayerDialog()
        {
            ShowSetupPlayerAction.Invoke();
            var player = Player.InitNewPlayer(null, null);
            return player;
        }
    }       
}
