﻿using System;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Client.WPF.Views;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;

namespace Client.WPF
{
    class Bootstrapper : UnityBootstrapper
    {
        private IRegionManager _regionManager;

        public Bootstrapper()
        {
            
        }
        
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            ConfigureContainer();

            _regionManager = Container.Resolve<IRegionManager>();

            InitViews();
            Application.Current.MainWindow.Show();
        }

        private void InitViews()
        {
            _regionManager.RegisterViewWithRegion("InfoRegion", typeof(InfoView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(FactoriesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(MinesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(WarehouseView));
        }
    }
}
