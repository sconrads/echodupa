﻿using System;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Client.WPF.Views;
using System.Windows;
using Application.Managers;
using Client.WPF.Navigation;
using Common.Interfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;

namespace Client.WPF
{
    class Bootstrapper : UnityBootstrapper
    {
        private IRegionManager _regionManager;

        public Bootstrapper()
        {
            
        }
        
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            ConfigureContainer();

            _regionManager = Container.Resolve<IRegionManager>();

            var gameManager = Container.Resolve<IGameManager>();
            gameManager.InitPlayer();

            InitViews();
            System.Windows.Application.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<IGameManager, GameManager>();
            Container.RegisterType<INavigationManager, NavigationManager>();
        }

        private void InitViews()
        {
            _regionManager.RegisterViewWithRegion("InfoRegion", typeof(InfoView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(FactoriesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(MinesView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(WarehouseView));
        }
    }
}
