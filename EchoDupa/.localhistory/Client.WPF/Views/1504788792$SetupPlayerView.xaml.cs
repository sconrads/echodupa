﻿using System;
using System.Windows.Controls;
using Prism.Interactivity.InteractionRequest;

namespace Client.WPF.Views
{
    /// <summary>
    /// Interaction logic for SetupPlayerView
    /// </summary>
    public partial class SetupPlayerView : UserControl, IInteractionRequestAware 
    {
        public SetupPlayerView()
        {
            InitializeComponent();
        }

        public INotification Notification { get; set; }
        public Action FinishInteraction { get; set; }
    }
}
