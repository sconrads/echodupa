﻿using Microsoft.Practices.Unity;
using Prism.Unity;
using Client.WPF.Views;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;

namespace Client.WPF
{
    class Bootstrapper : UnityBootstrapper
    {
        private readonly IRegionManager _regionManager;

        public Bootstrapper()
        {
            _regionManager = Container.Resolve<IRegionManager>();
        }
        
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
            _regionManager.RegisterViewWithRegion("InfoRegion", typeof(InfoView));
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(InfoView));
        }
    }
}
