﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Business.Entities;

namespace Client.WPF.ViewModels
{
    public class SetupPlayerViewModel : BindableBase
    {
        public SetupPlayerViewModel()
        {

        }

        public string Name { get; set; }
        public List<Resource> SourceResources { get; set; }
        public Resource Resource1 { get; set; }
        public Resource Resource2 { get; set; }
    }
}
