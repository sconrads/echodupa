﻿using Prism.Mvvm;

namespace Client.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Echo Dupa - the best Game";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel()
        {

        }
    }
}
