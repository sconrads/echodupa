﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Business.Entities;
using Common.Interfaces;


namespace Client.WPF.ViewModels
{
    public class MinesViewModel : BindableBase
    {
        private readonly IGameManager _gameManager;

        public MinesViewModel(IGameManager gameManager)
        {
            _gameManager = gameManager;
            MinesSource = new ObservableCollection<Mine>(_gameManager.ActivePlayer.Mines);
        }

        public string HeaderInfo => Common.ResourcesPL.Mines;
        public ObservableCollection<Mine> MinesSource { get; set; }
        private Mine _selectedMine;

        public Mine SelectedMine
        {
            get { return _selectedMine; }
            set
            {
                SetProperty(ref _selectedMine, value);
            }
        }
        
    }
}