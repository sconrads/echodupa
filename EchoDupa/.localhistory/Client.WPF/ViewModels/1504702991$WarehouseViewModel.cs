﻿
using Common;
using Prism.Mvvm;


namespace Client.WPF.ViewModels
{
    public class WarehouseViewModel : BindableBase
    {
        public WarehouseViewModel()
        {

        }

        public string HeaderInfo => ResourcesPL.Warehouse;
    }
}
