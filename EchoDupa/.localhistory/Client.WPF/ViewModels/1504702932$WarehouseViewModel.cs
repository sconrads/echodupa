﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace Client.WPF.ViewModels
{
    public class WarehouseViewModel : BindableBase
    {
        public WarehouseViewModel()
        {

        }

        public string HeaderInfo => ResourcesPL.Warehouse;
    }
}
