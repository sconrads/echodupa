﻿using System;
using Prism.Mvvm;
using Prism.Regions;

namespace Client.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private string _title = "Echo Dupa - the best Game";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _warehouseSelected;

        public bool WarehouseSelected
        {
            get { return _warehouseSelected; }
            set
            {
                _warehouseSelected = value;
                if (value)
                {
                    _regionManager.RequestNavigate("ContentRegion", new Uri("WarehouseView", UriKind.Relative));
                }
            }
        }


        public MainWindowViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }
    }
}
