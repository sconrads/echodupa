﻿using System;
using Common.Interfaces;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;

namespace Client.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly INavigationManager _navigationManager;
        private string _title = "Echo Dupa - the best Game";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        public InteractionRequest<Notification> NewPlayerPopupRequest { get; set; }

        public MainWindowViewModel(IRegionManager regionManager, INavigationManager navigationManager)
        {
            _regionManager = regionManager;
            _navigationManager = navigationManager;

            NewPlayerPopupRequest = new InteractionRequest<Notification>();

            _navigationManager.ShowSetupPlayer = () => NewPlayerPopupRequest.Raise(new Notification{Title = "test", Content = "dupa"}, r => Title = "dupa");
        }
    }
}
