﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Business.Entities;

namespace Client.WPF.ViewModels
{
    public class MinesViewModel : BindableBase
    {
        public MinesViewModel()
        {

        }

        public string HeaderInfo => Common.ResourcesPL.Mines;
        public ObservableCollection<Mine> MinesSource { get; set; }

    }
}