﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Business.Entities;


namespace Client.WPF.ViewModels
{
    public class MinesViewModel : BindableBase
    {
        public MinesViewModel()
        {
            MinesSource = new ObservableCollection<Mine>()
            {
                new Mine()
                { Name = "Kopalnia Węgla", NrOfWorkers = 100, Level = 2, Resource = Common.Dictionaries.Wares.ResourcesDictionary[Common.ResourcesPL.ResourceCoal]},
                new Mine()
                { Name = "Kopalnia Żelazo", NrOfWorkers = 300, Level = 12, Resource = Common.Dictionaries.Wares.ResourcesDictionary[Common.ResourcesPL.ResourceIron]},
                new Mine()
                { Name = "Tartak", NrOfWorkers = 10, Level = 0, Resource = Common.Dictionaries.Wares.ResourcesDictionary[Common.ResourcesPL.ResourceWood]},
            };
        }

        public string HeaderInfo => Common.ResourcesPL.Mines;
        public ObservableCollection<Mine> MinesSource { get; set; }
        private Mine _selectedMine;

        public Mine SelectedMine
        {
            get { return _selectedMine; }
            set
            {
                SetProperty(ref _selectedMine, value);
            }
        }
        
    }
}