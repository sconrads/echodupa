﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Client.WPF.ViewModels
{
    public class MinesViewModel : BindableBase
    {
        public MinesViewModel()
        {

        }

        public string HeaderInfo => Common.ResourcesPL.Mines;
    }
}