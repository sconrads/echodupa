﻿using System;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;

namespace Client.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private string _title = "Echo Dupa - the best Game";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        public InteractionRequest<Notification> NewPlayerPopupRequest { get; set; }

        public MainWindowViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;

            NewPlayerPopupRequest = new InteractionRequest<Notification>();

            NewPlayerPopupRequest.Raise(new Notification() {Title = "test", Content = "dupa"}, r => Title = "dupa");
        }
    }
}
