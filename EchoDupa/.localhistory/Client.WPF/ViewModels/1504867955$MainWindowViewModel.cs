﻿using System;
using System.Windows.Media.Animation;
using Business.Entities;
using Common.Interfaces;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;

namespace Client.WPF.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly INavigationManager _navigationManager;
        private string _title = "Echo Dupa - the best Game";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        public InteractionRequest<Notification> NewPlayerPopupRequest { get; set; }

        public MainWindowViewModel(IRegionManager regionManager, INavigationManager navigationManager)
        {
            _regionManager = regionManager;
            _navigationManager = navigationManager;

            NewPlayerPopupRequest = new InteractionRequest<Notification>();

            _navigationManager.ShowSetupPlayerAction = ShowView;
        }


        public Player ShowView()
        {
            Player result = null;
            NewPlayerPopupRequest.Raise(new Notification {Title = "test"}, x=> {result = x.Content as Player;});
            return result;
        }


    }
}
