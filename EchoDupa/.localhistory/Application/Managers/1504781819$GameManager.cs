﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        public Player InitPlayer()
        {
            Player player;
            if (File.Exists(Common.Consts.SaveDataPath))
            {
                player = new Player();
            }
            else
            {
                var mines = new List<Mine>
                {
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Coal},
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Wood},
                };
                player = Player.InitNewPlayer(mines);
                using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
                using (MemoryStream ms = new MemoryStream())
                {
                    BinaryFormatter bf = new BinaryFormatter();

                    bf.Serialize(ms, player);
                    byte[] bytes = new byte[ms.Length];
                    ms.Read(bytes, 0, (int) ms.Length);
                    ms.CopyTo(file);
                    ms.Close();
                }
            }
            return new Player();
        }
    }
}