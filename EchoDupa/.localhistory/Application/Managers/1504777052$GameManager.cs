﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        public Player InitPlayer()
        {
            if (File.Exists(Common.Consts.SaveDataPath))
            {
            }
            else
            {
                using (FileStream file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
                using (MemoryStream ms = new MemoryStream())
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(ms, new Player());
                    byte[] bytes = new byte[ms.Length];
                    ms.Read(bytes, 0, (int) ms.Length);
                    file.Write(bytes, 0, bytes.Length);
                    ms.Close();
                }
            }
            return new Player();
        }
    }
}