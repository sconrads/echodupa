﻿using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        public Player InitPlayer()
        {
            Player player;
            if (File.Exists(Common.Consts.SaveDataPath))
            {
                using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    player = (Player) bf.Deserialize(file);
                }
            }
            else
            {
                var mines = new List<Mine>
                {
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Coal},
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Wood},
                };
                player = Player.InitNewPlayer(mines);
                using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
                
                {
                    BinaryFormatter bf = new BinaryFormatter();

                    bf.Serialize(file, player);
                    file.Close();
                }
            }
            return player;
        }
    }
}