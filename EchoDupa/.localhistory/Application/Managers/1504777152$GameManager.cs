﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        public Player InitPlayer()
        {
            if (File.Exists(Common.Consts.SaveDataPath))
            {
            }
            else
            {
                using (FileStream file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
                using (MemoryStream ms = new MemoryStream())
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    var mines = new List<Mine>
                    {
                        new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Coal},
                        new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Wood},
                    };
                    bf.Serialize(ms, Player.InitNewPlayer(mines));
                    byte[] bytes = new byte[ms.Length];
                    ms.Read(bytes, 0, (int) ms.Length);
                    file.Write(bytes, 0, bytes.Length);
                    ms.Close();
                }
            }
            return new Player();
        }
    }
}