﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    public class Factory : Facility
    {
        public Product Product { get; set; }
        public long RequestCount { get; set; }


        public long Make(TimeSpan timeSpan)
        {
            var productionSpeed =  Product.ProductionSpeed * Efficiency * NrOfWorkers;
            var madeProductsCount = productionSpeed * timeSpan.Ticks;

            if (RequestCount < madeProductsCount)
            {
                var tmp = RequestCount;
                RequestCount = 0;
                Product = null;
                return tmp;
            }

            RequestCount -= (long)madeProductsCount;
            return (long) madeProductsCount;
        }

        public static void CreateMine(ref long money, ref List<Factory> factories)
        {
            if (GetNewFacilityCost(factories.Count) > money)
                throw new Exceptions.FacilityTooExpensiveExeption();

            money -= GetNewFacilityCost(factories.Count);
            factories.Add(new Factory()
            {
                Level = 1,
                NrOfWorkers = 0,
                Efficiency = 1,
            });
        }

    }
}