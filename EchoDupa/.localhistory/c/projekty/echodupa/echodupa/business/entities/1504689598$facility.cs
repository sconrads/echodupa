﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entities
{
    public class Facility
    {
        public string Name { get; set; }
        public double Efficiency { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }

        public long UpgradeCost => 10 ^ Level;

        public void IncreaseLevel(long money)
        {
            Level++;
            //todo
            Efficiency *= 1.10;
        }
    }
}
