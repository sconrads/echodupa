﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    public class Factory
    {
        public string Name { get; set; }
        public Product Product { get; set; }
        public double Efficiency { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }
        public int ProductQuantity { get; set; }


        public long Make(TimeSpan timeSpan, WarehouseContainer productContainer, List<WarehouseContainer> componentsContainers)
        {
            var productionSpeed =  Product.ProductionSpeed * Efficiency * NrOfWorkers;

            var madeProductsCount = productionSpeed * timeSpan.Ticks;
        }
    }
}