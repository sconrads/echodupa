﻿using System;

namespace Business.Entities
{
    public class Mine : Facility
    {
        public Resource Resource { get; set; }

        public long Colect(TimeSpan timeSpan)
        {
            return (long)(Resource.MiningSpeed * Efficiency * NrOfWorkers * timeSpan.Ticks);
        }

        public static Mine CreateMine(long money)
        {
            return new Mine();
        }
    }
}