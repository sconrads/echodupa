﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    public class Factory
    {
        public string Name { get; set; }
        public Product Product { get; set; }
        public double Efficiency { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }
        public long RequestCount { get; set; }


        public long Make(TimeSpan timeSpan)
        {
            var productionSpeed =  Product.ProductionSpeed * Efficiency * NrOfWorkers;
            var madeProductsCount = productionSpeed * timeSpan.Ticks;

            if (RequestCount < madeProductsCount)
            {
                var tmp = RequestCount;
                RequestCount = 0;
                Product = null;
                return tmp;
            }

            RequestCount -= (long)madeProductsCount;
            return (long) madeProductsCount;
        }

    }
}