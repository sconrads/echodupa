﻿namespace Business.Entities
{
    public class Factory
    {
        public string Name { get; set; }
        public Product Product { get; set; }
        public double Efficiency { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }

    }
}