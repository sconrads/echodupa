﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    public class Mine : Facility
    {
        public Resource Resource { get; set; }

        public long Colect(TimeSpan timeSpan)
        {
            return (long)(Resource.MiningSpeed * Efficiency * NrOfWorkers * timeSpan.Ticks);
        }

        public static void CreateMine(ref long money, ref List<Mine> mines)
        {
            if (GetNewFacilityCost(mines.Count) > money)
                throw new Exceptions.FacilityTooExpensiveExeption();


                money -= GetNewFacilityCost(mines.Count);
                mines.Add(new Mine());
            
        }


    }
}