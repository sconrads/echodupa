﻿using System;
using System.Collections.Generic;

namespace Business.Entities
{
    public class Factory : Facility
    {
        public Product Product { get; set; }
        public long RequestCount { get; set; }


        public long Make(TimeSpan timeSpan)
        {
            var productionSpeed =  Product.ProductionSpeed * Efficiency * NrOfWorkers;
            var madeProductsCount = productionSpeed * timeSpan.Ticks;

            if (RequestCount < madeProductsCount)
            {
                var tmp = RequestCount;
                RequestCount = 0;
                Product = null;
                return tmp;
            }

            RequestCount -= (long)madeProductsCount;
            return (long) madeProductsCount;
        }

        public static void CreateMine(ref long money, ref List<Mine> mines)
        {
            if (GetNewFacilityCost(mines.Count) > money)
                throw new Exceptions.FacilityTooExpensiveExeption();

            money -= GetNewFacilityCost(mines.Count);
            mines.Add(new Mine()
            {
                Level = 1,
                NrOfWorkers = 0,
                Efficiency = 1,
            });
        }

    }
}