﻿using System.Collections.Generic;

namespace Business.Entities
{
    public class Product : Ware
    {
        public double ProductionSpeed { get; set; }
        public Dictionary<Ware, int> Components { get; set; }
    }
}