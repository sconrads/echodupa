﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Business.Entities;
using Common.Interfaces;
using Prism.Interactivity.InteractionRequest;

namespace Client.WPF.ViewModels
{
    public class SetupPlayerViewModel : BindableBase, IInteractionRequestAware
    {
        private readonly IGameManager _gameManager;
        private string _name;

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public List<Resource> SourceResources { get; set; }
        public Resource Resource1 { get; set; }
        public Resource Resource2 { get; set; }

        private ICommand ApplyCommand { get; set; }

        public SetupPlayerViewModel(IGameManager gameManager)
        {
            _gameManager = gameManager;
            ApplyCommand = new DelegateCommand(Apply);
        }

        private void Apply()
        {
            _gameManager.InitPlayer();
            FinishInteraction.Invoke();

        }

        public INotification Notification { get; set; }
        public Action FinishInteraction { get; set; }
    }
}
