﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        private readonly INavigationManager _navigationManager;

        public GameManager(INavigationManager navigationManager)
        {
            _navigationManager = navigationManager;
        }

        public Player InitPlayer()
        {
            Player player;
            if (File.Exists(Common.Consts.SaveDataPath))
            {
                player = LoadPlayer();
            }
            else
            {
                _navigationManager.ShowSetupPlayerDialog();
                var mines = new List<Mine>
                {
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Coal},
                    new Mine() {Level = 0, Resource = Common.Dictionaries.Wares.Wood},
                };
                player = Player.InitNewPlayer(mines, null);
                SavePlayer(player);
            }
            return player;
        }

        public void SavePlayer(Player player)
        {
            using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, player);
                file.Close();
            }
        }

        public Player LoadPlayer()
        {
            using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Open, FileAccess.Read))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Player)formatter.Deserialize(file);
            }
        }
    }
}