﻿using System;
using System.Runtime.Serialization;

namespace Business.Exceptions
{
    [Serializable]
    internal class NotPlayersFacilityException : Exception
    {
        public NotPlayersFacilityException()
        {
        }

        public NotPlayersFacilityException(string message) : base(message)
        {
        }

        public NotPlayersFacilityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotPlayersFacilityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}