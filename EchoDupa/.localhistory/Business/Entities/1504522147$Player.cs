﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Entities
{
    public class Player
    {
        public string Name { get; set; }
        public Warehouse Warehouse { get; set; }
        public List<Factory> Factories { get; set; }
        public List<Mine> Mines { get; set; }
        public List<Offer> Offers { get; set; }
        public long NrOfPeople { get; set; }


        public void BuyPeople(long quantity)
        {
        }

        /// <summary>
        /// dodaje lub odejmuje ludzi z kopalni
        /// </summary>
        /// <param name="mine"></param>
        /// <param name="peopleToChange">wartość dodatnia lub ujemna oznacza ilość osób zmieniająca wielkość pracowników w kopalni</param>
        public void ChangePeopleCountInMine(Mine mine, long peopleToChange)
        {
            if (peopleToChange > NrOfPeople)
            {
                mine.NrOfWorkers += NrOfPeople;
                NrOfPeople = 0;
            }
            else if (peopleToChange < mine.NrOfWorkers)
            {
                NrOfPeople = mine.NrOfWorkers;
                mine.NrOfWorkers = 0;
            }
            else
            {
                mine.NrOfWorkers += peopleToChange;
                NrOfPeople -= peopleToChange;
            }
        }

        public void UpdateMines(TimeSpan timeSpan)
        {
            foreach (var mine in Mines)
            {
                var container = Warehouse.Containers.First(x => x.Ware == mine.Resource);
                container.FillContainer(mine.Colect(timeSpan));
            }
        }

        public void UpdateFactories(TimeSpan timeSpan)
        {
            foreach (var factory in Factories)
            {
                var product = factory.Product;
                
            }
        }
    }
}