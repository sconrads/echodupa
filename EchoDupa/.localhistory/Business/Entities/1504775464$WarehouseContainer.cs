﻿namespace Business.Entities
{
    public class WarehouseContainer
    {
        public Ware Ware { get; set; }
        public long Amount { get; set; }
        public long MaxAmount { get; set; }

        public void FillContainer(long amount)
        {
            Amount += amount;
            if (Amount > MaxAmount)
                Amount = MaxAmount;
        }
    }
}