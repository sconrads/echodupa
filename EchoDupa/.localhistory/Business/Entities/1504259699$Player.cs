﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Entities
{
    public class Player
    {
        public string Name { get; set; }
        public Warehouse Warehouse { get; set; }
        public List<Factory> Factories { get; set; }
        public List<Mine> Mines { get; set; }
        public List<Offer> Offers { get; set; }
        public long NrOfPeople { get; set; }


        public void BuyPeople(long quantity)
        {
            
        }
        /// <summary>
        /// dodaje lub odejmuje ludzi z kopalni
        /// </summary>
        /// <param name="mine"></param>
        /// <param name="peopleToChange">wartość dodatnia lub ujemna</param>
        public void ChangePeopleCountInMine(Mine mine,long peopleToChange)
        {
            if (peopleToChange > NrOfPeople)
            {
                return;
            }else if (peopleToChange < mine.NrOfWorkers)
            {
                return;
            }else
            {
                mine.NrOfWorkers += peopleToChange;
                NrOfPeople -= peopleToChange;
            }
        }

        public void UpdateMines(TimeSpan timeSpan)
        {
            foreach (var mine in Mines)
            {
                var container = Warehouse.Containers.First(x => x.Ware == mine.Resource);
                container.FillContainer(mine.Colect(timeSpan));
            }
        }

    }
}
