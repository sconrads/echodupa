﻿using System;

namespace Business.Entities
{
    public class Mine
    {
        public Resource Resource { get; set; }
        public double MiningSpeed { get; set; }
        public int Level { get; set; }
        public long NrOfWorkers { get; set; }

        public long Colect(TimeSpan timeSpan)
        {
            return (long)(Resource.MiningSpeed * MiningSpeed * NrOfWorkers * timeSpan.Ticks);
        }

        public void IncreaseLevel()
        {
            Level++;
        }
    }
}