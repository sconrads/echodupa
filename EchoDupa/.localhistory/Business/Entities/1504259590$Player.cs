﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Entities
{
    public class Player
    {
        public string Name { get; set; }
        public Warehouse Warehouse { get; set; }
        public List<Factory> Factories { get; set; }
        public List<Mine> Mines { get; set; }
        public List<Offer> Offers { get; set; }
        public long NrOfPeople { get; set; }


        public void BuyPeople(long quantity)
        {
            
        }

        public void UpdateMines(TimeSpan timeSpan)
        {
            foreach (var mine in Mines)
            {
                var container = Warehouse.Containers.First(x => x.Ware == mine.Resource);
                container.FillContainer(mine.Colect(timeSpan));
            }
        }

    }
}
