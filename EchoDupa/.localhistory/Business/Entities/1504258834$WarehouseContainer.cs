﻿namespace Business.Entities
{
    public class WarehouseContainer
    {
        public Ware Ware { get; set; }
        public long Amount { get; set; }
        public long MaxAmount { get; set; }
    }
}