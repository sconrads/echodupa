﻿using System;

namespace Business.Entities
{
    public class Resource : Ware
    {
        [Serializable]
        public double MiningSpeed { get; set; }
    }
}