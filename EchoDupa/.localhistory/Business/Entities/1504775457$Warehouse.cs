﻿using System.Collections.Generic;

namespace Business.Entities
{
    public class Warehouse
    {
        public List<WarehouseContainer> Containers { get; set; }
        public int Level { get; set; }

    }
}