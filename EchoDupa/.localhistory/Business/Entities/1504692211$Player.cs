﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Entities
{
    public class Player
    {
        private long _money;
        private List<Mine> _mines;
        private List<Factory> _factories;
        private Warehouse _warehouse;
        public string Name { get; set; }

        public Warehouse Warehouse
        {
            get { return _warehouse; }
            set { _warehouse = value; }
        }

        public List<Factory> Factories
        {
            get { return _factories; }
            set { _factories = value; }
        }

        public List<Mine> Mines
        {
            get { return _mines; }
            set { _mines = value; }
        }

        public List<Offer> Offers { get; set; }

        public long NrOfPeople { get; set; }

        public long Money
        {
            get { return _money; }
            set { _money = value; }
        }

        public void UpdatePlayer(TimeSpan timeSpan)
        {
            UpdateMines(timeSpan);
            UpdateFactories(timeSpan);
        }

        public void BuyPeople(long quantity)
        {
        }

        public void StartProduction(Factory factory, Product product, long requestCount)
        {
            factory.Product = product;
            factory.RequestCount = requestCount;

            foreach (var item in product.Components)
            {
                var container = Warehouse.Containers.First(x => x.Ware == item.Key);
                var decreaseValue = item.Value * requestCount;
                container.Amount -= decreaseValue;
            }
        }

        public void CancleProduction(Factory factory)
        {
            factory.CancelProduction(ref _warehouse);
        }

        public void ChangePeopleCountInFacility(Facility facility, long peopleToChange, ref long NrOfPeople)
        {
            facility.ChangePeopleCount(peopleToChange, ref NrOfPeople);
        }

        private void UpdateMines(TimeSpan timeSpan)
        {
            foreach (var mine in Mines)
            {
                var container = Warehouse.Containers.First(x => x.Ware == mine.Resource);
                container.FillContainer(mine.Colect(timeSpan));
            }
        }

        private void UpdateFactories(TimeSpan timeSpan)
        {
            foreach (var factory in Factories)
            {
                var container = Warehouse.Containers.First(x => x.Ware == factory.Product);
                container.FillContainer(factory.Make(timeSpan));
            }
        }

        public void AddFactory()
        {
            Factory.CreateFactory(ref _money, ref _factories);
        }

        public void AddMine(Resource resource)
        {
            Mine.CreateMine(ref _money, ref _mines, resource);
        }
    }
}