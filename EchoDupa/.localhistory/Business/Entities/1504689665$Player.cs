﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Entities
{
    public class Player
    {
        public string Name { get; set; }
        public Warehouse Warehouse { get; set; }
        public List<Factory> Factories { get; set; }
        public List<Mine> Mines { get; set; }
        public List<Offer> Offers { get; set; }
        public long NrOfPeople { get; set; }
        public long Money { get; set; }

        public void UpdatePlayer(TimeSpan timeSpan)
        {
            UpdateMines(timeSpan);
            UpdateFactories(timeSpan);
        }

        public void BuyPeople(long quantity)
        {
        }

        public void StartProduction(Factory factory, Product product, long requestCount)
        {
            factory.Product = product;
            factory.RequestCount = requestCount;
            
            foreach (var item in product.Components)
            {
                var container = Warehouse.Containers.First(x => x.Ware == item.Key);
                var decreaseValue = item.Value * requestCount;
                container.Amount -= decreaseValue;
            }
        }

        public void CancleProduction(Factory factory)
        {
            foreach (var item in factory.Product.Components)
            {
                var container = Warehouse.Containers.First(x => x.Ware == item.Key);
                container.FillContainer(item.Value * factory.RequestCount);
            }
            factory.RequestCount = 0;
            factory.Product = null;
        }

        public void ChangePeopleCountInFacility(Facility facility, long peopleToChange)
        {
            if (peopleToChange > NrOfPeople)
            {
                facility.NrOfWorkers += NrOfPeople;
                NrOfPeople = 0;
            }
            else if (peopleToChange < facility.NrOfWorkers)
            {
                NrOfPeople = facility.NrOfWorkers;
                facility.NrOfWorkers = 0;
            }
            else
            {
                facility.NrOfWorkers += peopleToChange;
                NrOfPeople -= peopleToChange;
            }
        }

        private void UpdateMines(TimeSpan timeSpan)
        {
            foreach (var mine in Mines)
            {
                var container = Warehouse.Containers.First(x => x.Ware == mine.Resource);
                container.FillContainer(mine.Colect(timeSpan));
            }
        }

        private void UpdateFactories(TimeSpan timeSpan)
        {
            foreach (var factory in Factories)
            {
                var container = Warehouse.Containers.First(x => x.Ware == factory.Product);
                container.FillContainer(factory.Make(timeSpan));
            }
        }

        public void AddFactory()
        {
            //todo walidacja, zabrać złoto, itp
            Factories.Add(new Factory());
        }

        public void AddMine()
        {
            //todo walidacja, zabrać złoto, itp
            Mine.CreateMine(ref Money, ref Mines);
        }
    }
}