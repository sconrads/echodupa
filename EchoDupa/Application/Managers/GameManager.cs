﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Business.Entities;
using Common.Interfaces;

namespace Application.Managers
{
    public class GameManager : IGameManager
    {
        private readonly INavigationManager _navigationManager;
        public Player ActivePlayer { get; private set; }

        public GameManager(INavigationManager navigationManager)
        {
            _navigationManager = navigationManager;
        }

        public void InitPlayer()
        {
            Player player;
            if (File.Exists(Common.Consts.SaveDataPath))
            {
                player = LoadPlayer();
            }
            else
            {
                player = _navigationManager.ShowSetupPlayerDialog();
                SavePlayer(player);
            }
            ActivePlayer = player;
        }

        public void SavePlayer(Player player)
        {
            using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Create, FileAccess.Write))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, player);
                file.Close();
            }
        }

        public Player LoadPlayer()
        {
            using (var file = new FileStream(Common.Consts.SaveDataPath, FileMode.Open, FileAccess.Read))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Player)formatter.Deserialize(file);
            }
        }
    }
}