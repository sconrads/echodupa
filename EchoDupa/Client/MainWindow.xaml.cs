﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using Microsoft.AspNet.SignalR.Client;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using Client.Annotations;
using Newtonsoft.Json.Linq;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public ObservableCollection<MessageDTO> Source { get; set; }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                _hub.Invoke("Echo", Guid, value);
                OnPropertyChanged(nameof(Text));
            }
        }

        public Guid Guid { get; set; }

        private IHubProxy _hub;
        private string _text;

        public HubConnection Connection { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Guid = Guid.NewGuid();
            Source = new ObservableCollection<MessageDTO>();
            this.Closing += OnClosing;

            InitConnection();
            RegisterIncomingMessages();

            _hub.Invoke("JoinUser", Guid, Connection.ConnectionId);
            
            
        }
        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            _hub.Invoke<Guid>("LeftUser", Guid);
        }

        private void InitConnection()
        {
            Connection = new HubConnection("http://localhost:51640/");
            _hub = Connection.CreateHubProxy("TestHub");
            Connection.Start().Wait();
        }

        private void RegisterIncomingMessages()
        {
            _hub.On("Echo", (Guid guid, string text) =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => OnUpdateChat(guid, text)));
            });

            _hub.On("JoinUser", (Guid guid) =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => OnJoinUser(guid)));
            });
            _hub.On("LeftUser", (Guid guid) =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => OnLeftUser(guid)));
            });
            _hub.On("GetActiveUsers", (object guids) =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => UpdateActiveUsers(guids)));
            });
        }

        private void UpdateActiveUsers(object guids)
        {
            var g = guids as JArray;
            var ee = g.ToList();
            foreach (var guid in ee)
            {
                if (!Source.Any(x => x.Guid.ToString() == guid.ToString()))
                {
                    Source.Add(new MessageDTO() { Guid = new Guid(guid.ToString()) });
                }
            }
        }

        private void OnLeftUser(Guid obj)
        {
            Source.Remove(Source.First(x => x.Guid == obj));
            OnPropertyChanged(nameof(Source));
        }

        private void OnJoinUser(Guid obj)
        {
            Source.Add(new MessageDTO {Guid = obj});
            OnPropertyChanged(nameof(Source));
        }

        private void OnUpdateChat(Guid arg1, string arg2)
        {
            Source.First(x => x.Guid == arg1).Text = arg2;
            OnPropertyChanged(nameof(Source));
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class MessageDTO : INotifyPropertyChanged
    {
        private Guid _guid;
        private string _text;

        public Guid Guid
        {
            get { return _guid; }
            set
            {
                _guid = value;
                OnPropertyChanged(nameof(Guid));
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
